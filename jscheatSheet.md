# JS cheatsheet

## Variables 

```js
let nomVar = 'Scope bloc';
const varConst = 'Scope bloc';
var nomVar = 'Scope fonction'
```
3 Types primitifs -> number, string, boolean

## Objet

En JSON :

```js
let myBook = {
    title: 'The Story of Tau',
    author: 'Will Alexander',
    numberOfPages: 250,
    isAvailable: true
};
```

## Classes

```js
class Book {
    constructor(title, author, pages){
        this.title = title;
        this.author = author;
        this.pages = pages;
    }
}
let myBook = new Book("L'histoire de Toto","Toto", 250);
const myBook2 = new Book(...) -> pointeur constant mais objet modifiable
```

## Collections

### Tableaux

```js
let guests = ["Sarah Kate", "Audrey Simon", "Will Alexander"];
guest.push("new"); guest.unshift("New");
guest.pop();
```

### Sets / Maps

```js
const set1 = new Set([1, 2, 3, 4, 5]);
console.log(set1.has(1));

const myMap = new Map();
const objectKey = {},
    functionKey = function () {},
    stringKey = "une chaîne";
myMap.set(stringKey, "valeur associée à 'une chaîne'");
myMap.set(objectKey, "valeur associée à objectKey");
myMap.set(functionKey, "valeur associée à functionKey");
```

**Objets et tableaux passés par référence**

## Controle

```js
for (let i = 0; i < numberOfPassengers; ++i){}
for (let i in passengers){} //i est un indice de tableau passengers[i]
for (let passenger of passengers){} //passenger est un objet (equiv. foreach)

switch (firstUser.accountLevel) {
    case 'normal':
        console.log('You have a normal account!');
        break;
    default:
        console.log('Unknown account type!');
}
```

## Erreurs

```js
try {
    // code susceptible à l'erreur ici
} catch (error) {
    // réaction aux erreurs ici
}
```

## Fonctions

```js
const nomFunc = (arg1, arg2) => {
    //corps
    return ...;
}

class ClassX{
    methodeInstance (args) {
        //do stuff
    }
    static methodeClasse(args){

    }
}
```

## Tests unitaires

Exemples

```js
describe('getWordCount()', function() {
    it('should find four words', function() {
        expect(getWordCount('I have four words!').to.equal(4));
    });
    it('should find no words', function() {
        expect(getWordCount('      ').to.equal(0));
    });
});
```
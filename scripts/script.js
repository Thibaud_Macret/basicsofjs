'use strict';

//Basics

console.log("Tests : ");

let today = new Date();

let formatDate = "Date : " + today.toDateString();
let selectDate = document.getElementById('date');
selectDate.textContent = formatDate;

let formatHour = today.getHours();
let greatings = formatHour > 12 ? 'Hello there!' : 'Good morning !';
let selectGreatings = document.getElementById('greatings');
selectGreatings.textContent = greatings;

//Function

function pow(number, pow)
{
    let result = Math.pow(number, pow);
    console.log(number + " puissance " + pow + " vaut " + result);
    return (result);
}

//Classes + bit of DOM

function Animal(name, age, sex, imagesrc)
{
    this.name = name;
    this.age = age;
    this.sex = (sex == "F" ? "Female" : "Male");
    this.imagesrc = imagesrc;
    this.display = DisplayAnimal;
}

function DisplayAnimal()
{
    console.log(this.name + " is a " + this.sex + " and is " + this.age + " years old.");
}

function Wolf(name, age, sex, imagesrc, furColor)
{
    Animal.call(this, name, age, sex, imagesrc);
    this.furColor = furColor;

    this.display = function(index){
        let paragraph = document.createElement('p');
        paragraph.appendChild(document.createTextNode(this.name + " is a " + this.furColor + " wolf."));
        document.getElementById("animalList").children[index].appendChild(paragraph);
        let img = document.createElement('img');
        img.src = 'imgs/' + this.imagesrc;
        img.height = 128;
        document.getElementById("animalList").children[index].appendChild(img);}
}

function Dog(name, age, sex, imagesrc, breed)
{
    Animal.call(this, name, age, sex, imagesrc);
    this.breed = breed;
    
    this.display = function(index){
        let paragraph = document.createElement('p');
        paragraph.appendChild(document.createTextNode(this.name + " is a " + this.breed + "."));
        document.getElementById("animalList").children[index].appendChild(paragraph);
        let img = document.createElement('img');
        img.src = 'imgs/' + this.imagesrc;
        img.height = 128;
        document.getElementById("animalList").children[index].appendChild(img);}
}

function Fox(name, age, sex, imagesrc, snow)
{
    Animal.call(this, name, age, sex, imagesrc);
    this.furColor = (snow == true ? "white" : "ginger");

    this.display = function(index){
        let paragraph = document.createElement('p');
        paragraph.appendChild(document.createTextNode(this.name + " is a " + this.furColor + " fox."));
        document.getElementById("animalList").children[index].appendChild(paragraph);
        let img = document.createElement('img');
        img.src = 'imgs/' + this.imagesrc;
        img.height = 128;
        document.getElementById("animalList").children[index].appendChild(img);}
}

function animalClick(pos)
{
    let nbelements = document.getElementById("animalList").children[pos].childElementCount;
    if(nbelements == 1)
    {
        switch(pos)
        {
            case 0:
                moro.display(0);
                break;
            case 1:
                agate.display(1);
                break;
            case 2:
                rox.display(2);
                break;
        }
    }else{
        for(let index = 0; index < nbelements-1; index++)
        {
            document.getElementById("animalList").children[pos].children[1].remove();
        }
    }
}

let moro = new Wolf("Moro", 17, "F", "Moro.png", "white");
let agate = new Dog("Agate", 14, "F", "Agate.jpg", "Golden retriever");
let rox = new Fox("Rox", 4, "M", "Rox.jpg" ,false);


//JSON
let JsonObj = {"surname":"John", "name":"Cina"};
console.log(JsonObj.name);
let JsonObjTab = [
        {"number":7, "color":"spades", "marked":false},
        {"number":2, "color":"diamond", "marked":false},
        {"number":5, "color":"spades", "marked":true},
        {"number":9, "color":"heart", "marked":false}
    ]
console.log(JsonObjTab[0], JsonObjTab[1], JsonObjTab[2], JsonObjTab[3]);


//Media Queries
function changeDocumentWithMediaQuery(query)
{
    if(query.matches)
    {
        console.log("Query is ok");
    }
    else
    {
        console.log("Query is !ok");
    }
}

var maxWidthQuery = window.matchMedia('(max-width: 700px)');
changeDocumentWithMediaQuery(maxWidthQuery);
maxWidthQuery.addListener(changeDocumentWithMediaQuery);